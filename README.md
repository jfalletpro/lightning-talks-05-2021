# Meetup Software Crafts·wo·manship Rennes
# Lightning talks - 20 mai 2021

## Liste des talks proposés :

* **[Titre du talk]** - [Speaker(s)] - [Description rapide]

* **Kanban : brossons le tableau** - Pascal Le Merrer

  La méthode Kanban pour l'IT ce n'est pas seulement un tableau avec des posts-its, loin de là.
  A vrai dire le tableau n'est même pas obligatoire.
  Nous allons revoir rapidement les fondamentaux de cette méthode,
  et pourquoi elle est bien adaptée au développement logiciel tel qu'on le pratique aujourd'hui.
